package com.springboot.internals;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaGuidesSpringBootTest2Application {

	public static void main(String[] args) {
		SpringApplication.run(JavaGuidesSpringBootTest2Application.class, args);
	}

}
